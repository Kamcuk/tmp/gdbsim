#!/bin/bash
set -euo pipefail

GDB=${1:-gdb}
executable=$2
shift 2
arguments=("$@")

{

set -x
cat <<EOF |
set confirm off
target sim
load
set args "${arguments[@]}"

break _Exit
break exit
break _exit

run

disassemble
info registers
print \$r0
print \$exitstatus

quit \$r0
EOF
tee /dev/stderr |
timeout 10 "$GDB" "$executable" && ret=$? || ret=$?
set +x

echo "RET: $GDB $executable ${arguments[*]} : $ret"

} 2>&1 | 
tee ${executable%%.*}.gdb

