#!/usr/bin/make
srcs=$(shell find . -type f -name '*.c' ! -name 'system.c')
objs=$(srcs:.c=.out)
gdbs=$(srcs:.c=.gdb)
qemus=$(srcs:.c=.qemu)

all: gdb_run
compile: $(objs) ;
gdb_run: $(gdbs)
	grep RET: $^ | sort -t: -k1 | column -t -s: -N"filename, ,command,exit value"
qemu_run: $(qemus) ;
%.gdb: %.out gdb_sim.sh
	./gdb_sim.sh arm-none-eabi-gdb $^
%.bin: %.out
	$(PREFIX)objcopy -O binary $^ $@
%.qemu: %.bin qemu_sim.sh
	./qemu_sim.sh $<
%.out: %.c system.c
	arm-none-eabi-gcc \
		-specs=rdimon.specs \
		-Wl,-wrap=main -ggdb3 -O0 \
		$^ -o $@
clean:
	rm -vf *.{out,bin,gdb,qemu,elf}

