#!/bin/bash
set -xeuo pipefail
timeout 2 qemu-system-arm -M versatilepb -m 128M -nographic -s -S -kernel "$1"

