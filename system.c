#include <stdio.h>
#include <stdlib.h>

void abort(void) {
	exit(0xDEEDBEEF);
}

int __real_main(int argc, char *argv[]);
int __wrap_main(int argc, char *argv[]) {
	const int ret =  __real_main(argc, argv);
	printf("879fc5d1-b39a-4625-b417-c640b8cb8b0f %d\n", ret);
	return ret;
}

